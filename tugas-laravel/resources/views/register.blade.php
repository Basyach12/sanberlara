<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SignUp</title>
</head>
<body>
    <h1>
        Buat Account Baru!
    </h1>
    <h3>Sign Up Form</h3>

    <form action="/home"method ="GET" >
        @csrf
        <label for="first">
            First Name
        </label>
        <br><br>
        <input type="text"placeholder="Input FirstName">
        <br><br>
        <label for="last">
            Last Name
        </label>
        <br><br>
        <input type="text"placeholder="Input LastName">
        <br><br>
        <label>Gender :</label>
        <br><br>
        <label><input type="radio" name="jenis_kelamin" value="laki-laki" /> Male</label><br>
        <label><input type="radio" name="jenis_kelamin" value="perempuan" /> Female</label><br>
        <label><input type="radio" name="jenis_kelamin" value="lainnya" /> Other</label>

        <br><br>
        <label>Nasionality:</label>
            <select name="kebangsaan">
                <option value="indonesia">Indonesia</option>
                <option value="inggris">England</option>
                <option value="singapore">Singapore</option>
                <option value="malaysia">Malaysia</option>
                <option value="australia">Australia</option>
                
            </select>

            <br><br>
            <label for="bahasa">Language Spoken: :</label><br>
            <label for="indonesia"><input type="checkbox" name="indonesia" id="1">Indonesia</label><br>
            <label for="english"><input type="checkbox" name="English" id="2">English</label><br>
            <label for="lainya"><input type="checkbox" name="lainya" id="1">other</label><br><br>
            <br><br>
            <label for="">Bio</label><br><br>
            <textarea name="bio" id="" cols="30" rows="10"></textarea>
            <br>
            <input type="submit" value="SignUp">

    </form>
</body>
</html>